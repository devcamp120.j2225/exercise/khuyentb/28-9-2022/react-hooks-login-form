import "bootstrap/dist/css/bootstrap.min.css"
import './App.css';
import TotalForm from "./Components/Form";

function App() {
  return (
    <div className="App">
      <TotalForm></TotalForm>
    </div>
  );
}

export default App;
