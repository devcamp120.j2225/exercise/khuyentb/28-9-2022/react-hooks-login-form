import { useEffect, useState } from "react"
import { Button, Col, Row, Input } from "reactstrap"

function SignUpForm({handleLoginFormProps}) {
    const onButtonLoginHandle = () => {
        handleLoginFormProps()
    }

    const [firstName, setFirstName ] = useState("");
    const [lastName, setLastName ] = useState("");
    const [email, setEmail ] = useState("");
    const [password, setPassword ] = useState("");
    
    const handleFirstNameInput = (e) => {
        setFirstName(e.target.value)
    }

    const handleLastNameInput = (e) => {
        setLastName(e.target.value)
    }

    const handleEmailInput = (e) => {
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e) => {
        setPassword(e.target.value)
    }

    useEffect(() => {
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
    })

    const onButtonGetStartedClick = () => {
        if (!firstName) {
            alert ("Please enter your firstname")
        } else if (firstName) {
            console.log("firstName: ", firstName)
        } else if (!lastName) {
            alert("Please enter your lastname")
        }else if (lastName) {
            console.log("lastName: ", lastName)
        } else if (!email) {
            alert("Please enter your email")
        } else if (email) {
            console.log("email: ", email )
        } else if (!password) {
            alert("Please enter your password")
        } else if (password) {
            console.log("password: ", password)
        }
    }

    return (
        <div className="largeBackGround">
            <div className="smaleBackGround">
                <Row>
                    <Col>
                        <Button className="loginButton">Sign Up</Button>
                        <Button className="signUpButton" onClick={onButtonLoginHandle}>Log In</Button>
                    </Col>
                </Row>
                <Row>
                    <h2 className="textWelcome">Sign Up For Free</h2>
                </Row>
                <Row>
                    <Col><Input placeholder="First Name*" className="input" onChange={handleFirstNameInput}></Input></Col>
                    <Col><Input placeholder="Last Name*" className="input" onChange={handleLastNameInput}></Input></Col>

                </Row>
                <Row>
                    <Input placeholder="Email Address*" className="input" onChange={handleEmailInput}></Input>
                </Row>
                <Row>
                    <Input placeholder="Set A Password**" className="input" onChange={handlePasswordInput}></Input>
                </Row>
                <Row>
                    <Button className="loginButtonLarge" onClick={onButtonGetStartedClick}>Get Started</Button>
                </Row>
            </div>
        </div>
    )
}

export default SignUpForm