import { useState } from "react";
import LoginForm from "./LoginForm/LoginForm";
import SignUpForm from "./SignUpForm/SignUpForm";

function TotalForm () {
    const [form, setForm ] =  useState(true)

    const handleSignUpForm = () => {
        setForm(true)
    }

    const handleLoginForm = () => {
        setForm(false)
    }

    return (
        <div>
            {form ? <SignUpForm handleLoginFormProps={handleLoginForm}></SignUpForm> : <LoginForm handleSignUpFormProps={handleSignUpForm}></LoginForm>}
        </div>
    )
}

export default TotalForm