import { useEffect, useState } from "react"
import { Button, Col, Row, Input } from "reactstrap"

function LoginForm({handleSignUpFormProps}) {
    const onButtonSignUpHandle = () => {
        handleSignUpFormProps()
    }

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleEmailInput = (e) => {
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e) => {
        setPassword(e.target.value)
    }

    useEffect(() => {
        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
    })

    const onButtonLoginClick = () => {
        if (!email) {
            alert ("Your email is invalid, please input again")
        } else if (email) {
            console.log("email: ", email )
        } else if (!password) {
            alert ("Please input your password")
        } else if (password) {
            console.log("password: ", password)
        }
    }
    return (
        <div className="largeBackGround">
            <div className="smaleBackGround">
                <Row>
                    <Col>
                        <Button className="signUpButton" onClick={onButtonSignUpHandle}>Sign Up</Button>
                        <Button className="loginButton">Log In</Button>
                    </Col>
                </Row>
                <Row>
                    <h2 className="textWelcome">Welcome Back!</h2>
                </Row>
                <Row>
                    <Input placeholder="Email Address*" className="input" onChange={handleEmailInput}></Input>
                </Row>
                <Row>
                    <Input placeholder="Passwords*" className="input" onChange={handlePasswordInput}></Input>
                </Row>
                <Row>
                    <p className="textForgotPassword">Forgot Passwords</p>
                </Row>
                <Row>
                    <Button className="loginButtonLarge" onClick={onButtonLoginClick}>Log In</Button>
                </Row>
            </div>
        </div>
    )
}

export default LoginForm